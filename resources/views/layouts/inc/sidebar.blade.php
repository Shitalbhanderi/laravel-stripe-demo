<div class="side-nav">
                <div class="side-nav-inner">
                    <div class="side-nav-logo">
                        <a href="index.html">
                            <!-- <div class="logo logo-dark" style="background-image: url('assets/images/logo/logo.png')"></div> -->
                            <!-- <div class="logo logo-white" style="background-image: url('assets/images/logo/logo-white.png')"></div> -->
                        </a>
                        <div class="mobile-toggle side-nav-toggle">
                            <a href="">
                                <i class="ti-arrow-circle-left"></i>
                            </a>
                        </div>
                    </div>
                    <ul class="side-nav-menu scrollable">
                        <li class="nav-item">
                            <a class="mrg-top-30" href="#">
                                <span class="icon-holder">
										<i class="ti-home"></i>
									</span>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item dropdown {{ request()->is('stripe*') ? 'open' : ''}}">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
										<i class="ti-package"></i>
									</span>
                                <span class="title">Stripe</span>
                                <span class="arrow">
										<i class="ti-angle-right"></i>
									</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="{{ request()->is('stripe/payment*') ? 'active' : ''}}">
                                    <a href="{{route('stripe.payment')}}">Payment</a>
                                </li>
                                <li class="{{ request()->is('stripe/subscription*') ? 'active' : ''}}">
                                    <a href="{{route('stripe.subscription.index')}}">Subscription</a>
                                </li>
                                <li class="{{ request()->is('stripe/shopping*') ? 'active' : ''}}">
                                    <a href="{{route('stripe.shopping.index')}}">Simple payments</a>
                                </li>
                               
                            </ul>
                        </li>
                        <li class="nav-item dropdown {{ request()->is('razorpay*') ? 'open' : ''}}">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
										<i class="ti-palette"></i>
									</span>
                                <span class="title">Razorpay</span>
                                <span class="arrow">
										<i class="ti-angle-right"></i>
									</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="{{ request()->is('razorpay/standard*') ? 'active' : ''}}">
                                    <a href="{{route('razorpay.standard.index')}}">Standard</a>
                                </li>
                                <li class="{{ request()->is('razorpay/custome*') ? 'active' : ''}}">
                                    <a href="{{route('razorpay.custome.index')}}">Custom</a>
                                </li>
                            </ul>
                        </li>
                        <!-- <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
										<i class="ti-file"></i>
									</span>
                                <span class="title">Forms</span>
                                <span class="arrow">
										<i class="ti-angle-right"></i>
									</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="form-elements.html">Form Elements</a>
                                </li>
                                <li>
                                    <a href="form-layouts.html">Form Layouts</a>
                                </li>
                                <li>
                                    <a href="form-wizard.html">Form Wizard</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
										<i class="ti-layout-media-overlay"></i>
									</span>
                                <span class="title">Tables</span>
                                <span class="arrow">
										<i class="ti-angle-right"></i>
									</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="basic-table.html">Basic Table</a>
                                </li>
                                <li>
                                    <a href="data-table.html">Data Table</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
										<i class="ti-pie-chart"></i>
									</span>
                                <span class="title">Charts</span>
                                <span class="arrow">
										<i class="ti-angle-right"></i>
									</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="nvd3.html">Nvd3</a>
                                </li>
                                <li>
                                    <a href="chartjs.html">ChartJs</a>
                                </li>
                                <li>
                                    <a href="sparkline.html">Sparkline</a>
                                </li>
                                <li>
                                    <a href="rickshaw.html">Ricksaw</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
										<i class="ti-map-alt"></i>
									</span>
                                <span class="title">Map</span>
                                <span class="arrow">
										<i class="ti-angle-right"></i>
									</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="google-map.html">Google Map</a>
                                </li>
                                <li>
                                    <a href="vector-map.html">Vector Map</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
										<i class="ti-cloud"></i>
									</span>
                                <span class="title">Extra</span>
                                <span class="arrow">
										<i class="ti-angle-right"></i>
									</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="invoice.html">Invoice</a>
                                </li>
                                <li>
                                    <a href="account.html">Account Settings</a>
                                </li>
                                <li>
                                    <a href="faq.html">FAQ</a>
                                </li>
                                <li>
                                    <a href="gallery.html">Gallery</a>
                                </li>
                                <li>
                                    <a href="sign-in.html">Sign In</a>
                                </li>
                                <li>
                                    <a href="sign-in2.html">Sign In 2</a>
                                </li>
                                <li>
                                    <a href="sign-up.html">Sign Up</a>
                                </li>
                                <li>
                                    <a href="404.html">404</a>
                                </li>
                                <li>
                                    <a href="500.html">500</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
										<i class="ti-view-list-alt"></i>
									</span>
                                <span class="title">Multiple Levels</span>
                                <span class="arrow">
										<i class="ti-angle-right"></i>
									</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="nav-item dropdown">
                                    <a href="javascript:void(0);">
                                        <span>Level 1.2</span>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="javascript:void(0);">
                                        <span>Level 1.1</span>
                                        <span class="arrow">
												<i class="ti-angle-right"></i>
											</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="javascript:void(0);">Level 2</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                </div>
            </div>