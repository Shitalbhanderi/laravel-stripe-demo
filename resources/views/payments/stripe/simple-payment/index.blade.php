@extends('layouts.master')

@section('content')
@if (Session::has('success'))
    <div class="alert alert-success text-center">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
        <p>{{ Session::get('success') }}</p>
    </div>
@endif
  
<form action="{{route('stripe.shopping.store')}}" method="post">
@method('POST')
@csrf
    @include('payments.products')
</form>



@endsection