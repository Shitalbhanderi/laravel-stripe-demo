@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card padding-30">
            <div class="row">
                <div class="col-md-12">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h4 class="card-title">Subscription</h4>

                    <div> {{ isset($userSubscription) ? 'Your plan -'.$userSubscription->plan_name : 'You are Subscribe to Any Plan'}} </div>
                    <div> {{ isset($userSubscription) ? 'Your plan will be expiry on '.\Carbon\Carbon::parse($userSubscription->subscription_ends_at)->format('d-m-Y') : '' }}</div> 

                    <div class="mrg-top-20">
                        <!-- <a href="" class="btn btn-default">Default</a> -->
                        @if(isset($subscription))
                        <a href="{{route('stripe.invoice',$userSubscription->invoice_id)}}" class="btn btn-success">Invoice</a>
                        @endif

                        @if(isset($subscription) && !$subscription->cancelled() && $subscription->stripe_status != "canceled")
                            <a href="{{route('stripe.change-plan')}}" class="btn btn-primary">Change Plan</a>
                        @endif
                        @if(isset($subscription) && !$subscription->cancelled())
                            <a href="{{route('stripe.cancel-subscription')}}" class="btn btn-warning">Cancle Subscription</a>
                        @endif
                        @if(isset($subscription) && $subscription->stripe_status != "canceled")
                            <a href="{{route('stripe.cancel-subscription-now')}}" class="btn btn-info">Resume Subscription</a>
                        @endif
                        @if(isset($subscription) && $subscription->cancelled() && $subscription->stripe_status != "canceled")
                            <a href="{{route('stripe.resume-subscription')}}" class="btn btn-danger">Cancle Now</a>
                        @endif
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

@endsection