@extends('layouts.master')
@push('page-css')
<style>
   .card-product .img-wrap {
   border-radius: 3px 3px 0 0;
   overflow: hidden;
   position: relative;
   height: 220px;
   text-align: center;
   }
   .card-product .img-wrap img {
   max-height: 100%;
   max-width: 100%;
   object-fit: cover;
   }
   .card-product .info-wrap {
   overflow: hidden;
   padding: 15px;
   border-top: 1px solid #eee;
   }
   .card-product .bottom-wrap {
   padding: 15px;
   border-top: 1px solid #eee;
   }
   .label-rating { margin-right:10px;
   color: #333;
   display: inline-block;
   vertical-align: middle;
   }
   .card-product .price-old {
   color: #999;
   }
</style>
@endpush

@section('content')

    @include('payments.products')

@endsection

@push('page-js')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
   $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
   }); 
   $('body').on('click', '.buy_now', function(e){
      var totalAmount = $(this).attr("data-amount");
      var product_id =  $(this).attr("data-id");

      var options = {
      "key": "{{env('RAZORPAY_KEY')}}",
      "amount": (totalAmount*100), // 2000 paise = INR 20
      "name": "test",
      "description": "Payment",
      "handler": function (response){
            $.ajax({
               url: "{{route('razorpay.pay-success')}}",
               type: 'post',
               dataType: 'json',
               headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               data: {
                  razorpay_payment_id: response.razorpay_payment_id , 
                  totalAmount : totalAmount ,
                  product_id : product_id,
                  razorpay_order_id: response.razorpay_order_id,
                  razorpay_signature_id : response.razorpay_signature,
               }, 
               success: function (response) {
                  window.location.href = "{{route('razorpay.success.redirect')}}";
               }
            });
         
      },
      "prefill": {
         "contact": '6353979444',
         "email":   'shital.9brainz@gmail.com',
      },
      "theme": {
         "color": "#528FF0"
      }
   };
   var rzp1 = new Razorpay(options);
   rzp1.open();
   e.preventDefault();
   });
   /*document.getElementsClass('buy_plan1').onclick = function(e){
      rzp1.open();
      e.preventDefault();
   }*/
</script>
@endpush
