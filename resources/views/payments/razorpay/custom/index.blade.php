@extends('layouts.master')

@section('content')
    @include('payments.products')
@endsection

@push('page-js')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script type="text/javascript" src="https://checkout.razorpay.com/v1/razorpay.js"></script>

<script>
var order_id;
// $(document).ready(function() { 
//     order_id = $("input[name=order_id]").val();
// });

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
}); 

var razorpay = new Razorpay({
    key: '{{ env('RAZORPAY_KEY'), env('RAZORPAY_SECRET') }}',
    image: 'https://i.imgur.com/n5tjHFD.png',
});

razorpay.once('ready', function(response) {
    console.log(response.methods);
});

var data = {
//   amount: 1000, // in currency subunits. Here 1000 = 1000 paise, which equals to ₹10
  currency: "INR",// Default is INR. We support more than 90 currencies.
  email: 'shital.9brainz@gmail.com',
  contact: '6353979444',
  notes: {
    address: '9Brainz,rajkot',
  },
  order_id: order_id,// Replace with Order ID generated in Step 4
  method: 'card',

  // method specific fields
  bank: 'HDFC'
};

data['card[name]'] = 'test';
data['card[number]'] = 4111111111111111;
data['card[cvv]'] = 123;
data['card[expiry_month]'] = 02;
data['card[expiry_year]'] = 2022;

$btn = $('.buy_now');
$('body').on('click', '.buy_now', function(e){
    var product_id =  $(this).attr("data-id");
    $.ajax({
        url: "{{route('razorpay.order.create')}}",
        type: 'post',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            amount : $(this).attr("data-amount"),
        }, 
        success: function (response) {
            console.log(response);
            data['order_id'] = response.order_id;
            data['amount'] = response.amount;

            razorpay.createPayment(data);
        }
    });

    razorpay.on('payment.success', function(response) {
        console.log(response);
        $.ajax({
            url: "{{route('razorpay.pay-success')}}",
            type: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                razorpay_payment_id: response.razorpay_payment_id , 
                totalAmount : data['amount'] ,
                product_id : product_id,
                razorpay_order_id: response.razorpay_order_id,
                razorpay_signature_id : response.razorpay_signature,
            }, 
            success: function (response) {
                // window.location.href = "{{route('razorpay.success.redirect')}}";
            }
        });
    });

    razorpay.on('payment.error', function(resp){alert(resp.error.description)}); // will pass error object to error handler

})

</script>
@endpush

