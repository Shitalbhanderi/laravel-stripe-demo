<div class="row">
@foreach($products as $product)
<div class="col-lg-4">
    <div class="card">
        <div class="card-media">
            <img class="img-responsive" src="{{$product->image}}" alt="" height="300px">
        </div>
        <div class="card-block">
            <h4 class="mrg-btm-20 no-mrg-top">Name : {{$product->name}}</h4>
            <p>
                Price : {{$product->price}} {{$product->currency}}
            </p>
            <div class="mrg-top-40">
                <!-- <a href="" class="btn btn-info no-mrg-btm buy-now">Buy Now</a> -->
                <input type="hidden" value="{{$product->id}}" name="id">
                <input type="submit" class="btn btn-info no-mrg-btm buy_now" value="Buy Now" data-amount="{{$product->price}}" data-id="{{$product->id}}">

            </div>
        </div>
    </div>
</div>
@endforeach
</div>