<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});


// Route::group(['middleware' => ['auth']], function(){
//     Route::get('home', 'HomeController@index');
    
//     Route::get('razorpay', 'HomeController@razorpayIndex')->name('razorpay.index');
//     Route::group(['prefix' => 'razorpay' , 'namespace' => 'razorpay'], function(){
//             Route::group(['prefix' => 'standard'], function(){
//                 Route::get('shopping', 'PaymentController@index')->name('razorpay.shopping');
//                 Route::post('pay-success', 'PaymentController@paySuccess')->name('razorpay.pay-success');
//                 Route::get('success-redirect', 'PaymentController@sucessRedirect')->name('razorpay.success.redirect');
//             });

//             Route::group(['prefix' => 'custom'], function() {
//                 Route::get('shopping', 'PaymentController@customIndex')->name('razorpay.custom.shopping');
//                 Route::post('pay-success', 'PaymentController@customPaySuccess')->name('razorpay.custom.pay-success');
//                 Route::get('success-redirect', 'PaymentController@sucessRedirect')->name('razorpay.custom.success.redirect');
//             });

//             // Route::get('my-store', 'RazorpayController@index')->name('razorpay.show');
//     });

//     Route::group(['prefix' => 'stripe'], function() {
//         Route::get('/', 'HomeController@stripeIndex')->name('stripe.index');
//         Route::get('payment','StripeController@payment')->name('payment');
//         Route::post('subscribe','StripeController@subscribe');

//         Route::group(['middleware' => 'check-subscription'], function() {
//             Route::get('subscription', 'HomeController@subscriptionIndex')->name('subcription.index');
//             Route::get('change-plan','StripeController@changePlan')->name('change-plan');
//             Route::get('cancel-subscription','StripeController@cancelSubscription')->name('cancel-subscription');
//             Route::get('cancel-subscription-now','StripeController@cancelSubscriptionNow')->name('cancel-subscription-now');
//             Route::get('resume-subscription','StripeController@resumeSubscription')->name('resume-subscription');
//             Route::get('invoice/{invoice}','StripeController@downloadInvoice')->name('invoice');
//         });

//         Route::group(['prefix' => 'shopping'], function () {
//             Route::get('/', 'HomeController@shoppingIndex')->name('shopping.index');
//             Route::get('checkout', 'ShoppingController@checkout')->name('shopping.checkout');
//             Route::post('checkout-store', 'ShoppingController@checkoutStore')->name('shopping.checkout.store');
//         });

//     });
    
//     Route::post('stripe\webhook', '\App\Http\Controllers\WebhookController@handleWebhook');

// });

Route::group(['middleware' => 'auth'], function() {

    Route::get('home','HomeController@index')->name('home');

    Route::group(['prefix' => 'stripe', 'namespace' => 'Stripe',], function() {
        Route::get('payment','SubscriptionController@payment')->name('stripe.payment');
        Route::post('subscribe','SubscriptionController@subscribe')->name('stripe.payment.store');

        Route::group(['middleware' => ['check-subscription']], function() {
            Route::get('subscription','SubscriptionController@index')->name('stripe.subscription.index');
            Route::get('change-plan','SubscriptionController@changePlan')->name('stripe.change-plan');
            Route::get('cancel-subscription','SubscriptionController@cancelSubscription')->name('stripe.cancel-subscription');
            Route::get('cancel-subscription-now','SubscriptionController@cancelSubscriptionNow')->name('stripe.cancel-subscription-now');
            Route::get('resume-subscription','SubscriptionController@resumeSubscription')->name('stripe.resume-subscription');
            Route::get('invoice/{invoice}','SubscriptionController@downloadInvoice')->name('stripe.invoice');
        });
        
        Route::get('shopping','ShoppingController@index')->name('stripe.shopping.index');
        Route::post('shopping', 'ShoppingController@store')->name('stripe.shopping.store');
        Route::get('checkout/{order_id}', 'ShoppingController@checkout')->name('stripe.shopping.checkout');
        Route::post('checkout-store', 'ShoppingController@checkoutStore')->name('stripe.shopping.checkout.store');

    });
    
    
    Route::group(['prefix' => 'razorpay' ,'namespace' => 'razorpay'], function() {
        Route::get('standard','PaymentController@standardIndex')->name('razorpay.standard.index');
        Route::get('custome','PaymentController@customeIndex')->name('razorpay.custome.index');
        Route::get('success-redirect', 'PaymentController@sucessRedirect')->name('razorpay.success.redirect');
        Route::post('pay-success', 'PaymentController@paySuccess')->name('razorpay.pay-success');
        Route::post('create-order', 'PaymentController@createOrder')->name('razorpay.order.create');


        // Route::group(['prefix' => 'standard'], function () {
        // });
        // Route::group(['prefix' => 'standard'], function () {
        // });

    });
    
});

Route::post('stripe\webhook', '\App\Http\Controllers\WebhookController@handleWebhook');

Auth::routes();
