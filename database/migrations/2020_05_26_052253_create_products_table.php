<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Product;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->integer('price');
            $table->string('currency');
            $table->string('image');

            $table->timestamps();
        });

        Product::create([
            'name' => 'php',
            'price' => 250,
            'currency' => 'INR',
            'image' => 'php.jpg',
        ]);

        Product::create([
            'name' => 'java',
            'price' => 500,
            'currency' => 'INR',
            'image' => 'java.jpg',
        ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
