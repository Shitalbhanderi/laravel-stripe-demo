<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStripeUserSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripe_user_subscriptions', function (Blueprint $table) {
            $table->id();

            $table->integer('user_id')->nullable();
            $table->string('plan_id')->nullable();
            $table->string('plan_name')->nullable();
            $table->integer('amount')->nullable();
            $table->string('subscription_id')->nullable();
            $table->string('customer_id')->nullable();
            $table->string('invoice_id')->nullable();
            $table->string('purchased_at')->nullable();
            $table->timestamp('subscription_ends_at')->nullable();
            $table->boolean('is_auto_renewal')->default(true);
            $table->string('invoice_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stripe_user_subscriptions');
    }
}
