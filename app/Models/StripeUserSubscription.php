<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Stripe\Invoice;
use Stripe\Stripe;

class StripeUserSubscription extends Model
{
    protected $fillable = ['user_id','amount','is_auto_renewal','subscription_ends_at','purchased_at','invoice_id','customer_id','subscription_id','plan_id','plan_name','invoice_number'];

    // public static $plans = [
    //     config('services.stripe.daily_plan_id') => 'Daily',
    //     config('services.stripe.monthly_plan_id') => "Monthly",
    //     config('services.stripe.yearly_plan_id') => 'Yearly',
    // ];

    public function getInvoiceData($invoiceId)
    {
        Stripe::setApiKey(config('services.stripe.secret'));

        return Invoice::retrieve($invoiceId);
    }

}
