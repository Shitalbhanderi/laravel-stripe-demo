<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','price','currency','image'];

    public function getImageAttribute($value)
    {
        return url('img/'.$value);
    }
}
