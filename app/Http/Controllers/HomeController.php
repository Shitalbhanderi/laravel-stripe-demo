<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserSubscription;
use Illuminate\Support\Collection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('home');
        return view('layouts.master');
    }

    public function stripeIndex()
    {
        return view('stripe.index');
    }

    public function razorpayIndex()
    {
        return view('razorpay.index');
    }

    public function subscriptionIndex()
    {
        $user = auth()->user();
       
        $userSubscription = UserSubscription::where('user_id',$user->id)->orderBy('id', 'DESC')->first();

        $subscription = $userSubscription ? $user->subscription($userSubscription->plan_name) : null;
        if($userSubscription){
            $data = [
                'plan' => $userSubscription->plan_name,
                'subscription' => $subscription,
                'userSubscription' => $userSubscription
            ];
        }

        return view('stripe.plan.index')->with($data);
    }

    public function shoppingIndex()
    {
        $products = new Collection([
            [
                'id' => 2,
                'name' => 'php book',
                'price' => 100,
                'currency' => 'usd',
                'image' => asset('img/php.jpg'),
            ],
            [
                'id' => 1,
                'name' => 'java book',
                'price' => 200,
                'currency' => 'usd',
                'image' => asset('img/java.jpg'),
            ]
        ]);
       
        $data = [
            'products' => $products,
        ];

        return view('stripe.shopping.index')->with($data);
    }

}
 