<?php

namespace App\Http\Controllers\Razorpay;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Models\RazorpayPayment;
use App\Models\Product;
use Razorpay\Api\Api;
use Session;


class PaymentController extends Controller
{
    public function index()
    {
        $products = new Collection([
            [
                'id' => 2,
                'name' => 'php book',
                'price' => 100,
                'currency' => 'usd',
                'image' => asset('img/php.jpg'),
            ],
            [
                'id' => 1,
                'name' => 'java book',
                'price' => 200,
                'currency' => 'usd',
                'image' => asset('img/java.jpg'),
            ]
        ]);
       
        $data = [
            'products' => $products,
        ];
        return view('razorpay.shopping')->with($data);
    }

    public function paySuccess(Request $request)
    {
        $user = auth()->user();
        $data = [
            'user_id' => $user->id,
            'payment_id' => $request->get('razorpay_payment_id'),
            'amount' => $request->get('totalAmount'),
        ];
        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
        if (!empty($request->razorpay_payment_id)) {
            $payment = $api->payment->fetch($request->razorpay_payment_id)->capture(array('amount' => $request->totalAmount));
        }

        RazorpayPayment::insert($data);
        return response()->json([
            'success' => true,
            // 'redirect_url' => redirect()->intended('razorpay.success.redirect')->getTargetUrl(),
        ]);

    }
    public function sucessRedirect()
    {
        Session::flash('success', 'Payment successful!');
        return back();
    }

    public function standardIndex()
    {
        $products = Product::all();
        return view('payments.razorpay.standard.index',compact('products'));
    }

    public function customeIndex()
    {
        $products = Product::all();
        return view('payments.razorpay.custom.index',compact('products'));
    }

    public function createOrder(Request $request)
    {
        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
        $order = $api->order->create(array('receipt' => '123231321654', 'amount' => $request->amount, 'currency' => 'INR')); // Creates order
        return response()->json([
            'order_id' => $order['id'],
            'amount' => $order['amount'],
        ]);
    }
}
