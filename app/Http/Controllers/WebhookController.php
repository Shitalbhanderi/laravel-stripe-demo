<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Laravel\Cashier\Subscription;
use App\Models\StripeUserSubscription;
use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;

class WebhookController extends CashierController
{
    /**
     * Handle subscription creation.
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleWebhook(Request $request)
    {
        $payload = $request->getContent();
        Log::info('webhook');

        $data = json_decode($payload, true);

        if (isset($data['type']) && $data['type'] == 'invoice.payment_succeeded')  {
            Log::info('test');
            Log::info($data);

            $customerId = $data['data']['object']['customer'];
            $subscriptionId = $data['data']['object']['subscription'];
            $invoiceId = $data['data']['object']['id'];
            $planId = $data['data']['object']['lines']['data'][0]['plan']['id'];

            $invoiceData = (new StripeUserSubscription())->getInvoiceData($invoiceId);

            if ( $user = $this->getUserByStripeId( $data['data']['object']['customer'] ) ) {
                $plans = [
                    config('services.stripe.daily_plan_id') => 'Daily',
                    config('services.stripe.monthly_plan_id') => "Monthly",
                    config('services.stripe.yearly_plan_id') => 'Yearly',
                ];
                $plan_name = isset( $plans[$planId] ) ? $plans[$planId] : false;

                if($planId == config('services.stripe.daily_plan_id')){
                    $subscription_ends_at = Carbon::now()->addDay()->format('Y-m-d H:i:s');
                }elseif($planId == config('services.stripe.monthly_plan_id')){
                    $subscription_ends_at = Carbon::now()->addMonth()->format('Y-m-d H:i:s');
                }else{
                    $subscription_ends_at = Carbon::now()->addYear()->format('Y-m-d H:i:s');
                }

                StripeUserSubscription::create([
                    'user_id' => $user->id,
                    'amount' => isset($data['data']['object']['lines']['data'][0]['plan']['amount'])
                    ? $data['data']['object']['lines']['data'][0]['plan']['amount']/100
                    : 0,
                    'subscription_id' => $subscriptionId,
                    'customer_id' => $customerId,
                    'invoice_id' => $invoiceId,
                    'invoice_number' => isset($invoiceData['number'])
                            ? $invoiceData['number']
                            : null,
                    'purchased_at' => Carbon::now(),
                    'subscription_ends_at' => $subscription_ends_at,
                    'is_auto_renewal' => true,
                    'plan_id' => $planId,
                    'plan_name' => $plan_name,
                ]);
            }

        }
    }
  
}