<?php

namespace App\Http\Controllers\Stripe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StripeUserSubscription;
use Stripe\Stripe;
use Stripe\Subscription;

class SubscriptionController extends Controller
{
    protected $user,$subscription,$userSubscription; 

    public function getUser()
    {
        $this->user = auth()->user();
        $this->userSubscription = UserSubscription::where('user_id',$this->user->id)->orderBy('id', 'DESC')->first();
        if($this->userSubscription){
                Stripe::setApiKey(config('services.stripe.secret'));
                // $this->subscription = Subscription::retrieve($this->userSubscription->subscription_id);
                $this->subscription = $this->user->subscription($this->userSubscription->plan_name);
        }
    }

    public function index()
    {
        $user = auth()->user();
        $userSubscription = StripeUserSubscription::where('user_id',$user->id)->orderBy('id', 'DESC')->first();

        $subscription = $userSubscription ? $user->subscription($userSubscription->plan_name) : null;
        $data = [];
        if($userSubscription){
            $data = [
                'plan' => $userSubscription->plan_name,
                'subscription' => $subscription,
                'userSubscription' => $userSubscription
            ];
        }

        return view('payments.stripe.subscription.index')->with($data);
    }

    public function payment()
    {
        $user = auth()->user();

        $availablePlans = [
            config('services.stripe.daily_plan_id') => 'Daily',
            config('services.stripe.monthly_plan_id') => "Monthly",
            config('services.stripe.yearly_plan_id') => 'Yearly',
        ];

        $data = [
            'intent' => auth()->user()->createSetupIntent(),
            'plans' => $availablePlans,       
        ];
        
        // return view ('stripe.plan.payment')->with($data);

        return view ('payments.stripe.subscription.payment')->with($data);
    }

    public function subscribe(Request $request)
    {
        // dd($request->all());
        $user = auth()->user();
        $paymentMethod = $request->payment_method;

        $planId = $request->plan;
        $subscription = $user->newSubscription($request->plan_name, $planId)->create($paymentMethod);
       
        return response([
            'success_url' => redirect()->intended('stripe\subscription')->getTargetUrl(),
            // 'success_url' => redirect()->route('stripe.subscription.index'),
            'status' => 'success'
        ]);
    }

    public function changePlan()
    {
        $this->getuser();

        if($this->userSubscription){
            if($this->userSubscription->stripe_plan == config('services.stripe.monthly_plan_id')){
                $planId = config('services.stripe.yearly_plan_id');
            }else{
                $planId = config('services.stripe.monthly_plan_id');
            }
            $this->userSubscription->swap($planId);
        }

        return redirect('home');
    }

    public function cancelSubscription()
    {
        $this->getuser();
        $this->subscription->cancel();
        return redirect('home');
    }

    public function cancelSubscriptionNow()
    {
        $this->getuser();
        $this->subscription->cancelNow();

        return redirect('home');

    }

    public function resumeSubscription()
    {
        $this->getuser();
        $this->subscription->resume();

        return redirect('home');
    }

    public function downloadInvoice($invoiceId)
    {
        $this->getuser();

        $invoice = \Stripe\Invoice::retrieve($this->userSubscription->invoice_id);
        // dd($invoice);
        // $invoice->sendInvoice();
        return $this->user->downloadInvoice($invoiceId, [
            'vendor' => 'Your Company',
            'product' => 'Your Product',
        ]);
    }
}
