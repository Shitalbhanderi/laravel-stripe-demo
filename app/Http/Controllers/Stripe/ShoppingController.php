<?php

namespace App\Http\Controllers\Stripe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use Session;
use Stripe;

class ShoppingController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('payments.stripe.simple-payment.index',compact('products'));
    }

    public function store(Request $request)
    {   
        $request->has('id') ? $id = $request->get('id') : $id = 0; 

        $product = Product::findOrFail($id);

        $order = Order::create([
            'product_id' => $product->id,
            'amount' => $product->price,
        ]);

        return redirect()->route('stripe.shopping.checkout',$order->id);
    }

    public function checkout($id)
    {
        $order = Order::findOrFail($id);

        return view('payments.stripe.simple-payment.payment',compact('order'));
    }

    public function checkoutStore(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => $request->get('amount') * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment laravel stripe demo." 
        ]);
  
        Session::flash('success', 'Payment successful!');
        return redirect()->route('stripe.shopping.index');
    }
}
