<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use App\Models\StripeUserSubscription;
use Closure;

class CheckSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user(); 
        $userSubscription = StripeUserSubscription::where('user_id',$user->id)->orderBy('id', 'DESC')->first();
        if($userSubscription)
        {
            if (!$userSubscription->subscription_ends_at) {
                session()->flash('error', 'Payment is required!');
    
                return redirect()->route('stripe.payment');
            }
    
            if ($userSubscription->subscription_ends_at < Carbon::now()) {
                // dd('id');
                session()->flash('error', 'Your subscription has been expired. You have to pay again!');
    
                return redirect()->route('stripe.payment');
            }
    
            // if (!$userSubscription->is_auto_renewal) {
            //     session()->flash('error', 'Please update your card details.');
    
            //     return redirect()->route('stripe.payment');
            // }
            return $next($request);
        }else{
            return redirect()->route('stripe.payment');
        }
    }
}
